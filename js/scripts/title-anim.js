//anima title
const reduceTitle = (e) => {
    if (document.querySelector(".big-header")) {
        document.querySelector(".big-header").classList.remove("big-header")
    }
}

document.addEventListener("scroll", reduceTitle)
setTimeout(() => {
    reduceTitle()
}, 1000);

document.querySelector(".neon-title-container").addEventListener("click", e => {
    e.currentTarget.classList.toggle('black')
})

window.addEventListener("scroll", e => {
    console.log(window.pageYOffset)
    window.requestAnimationFrame(() => {
        if (window.pageYOffset > 200) {
           document.querySelector("header").classList.add("transparent")
        } else {
            document.querySelector("header").classList.remove("transparent")
        }
    })
})