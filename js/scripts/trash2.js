const blender = document.querySelector('#blender')
const playBtn = document.querySelector(".btn")
const allSidesBtn = document.querySelectorAll(".side-btn")
let blenderYPosition = parseInt(`${blender.style.top}`, 10)
let blenderXPosition = parseInt(`${blender.style.left}`, 10)
let positionTopImg = 0
let positionLeftImg = 0
let refreshIntervalId = ""
let show = document.querySelector(".student-works")

const reduceTitle = (e) => {
    if (document.querySelector(".big-header")) {
        document.querySelector(".big-header").classList.remove("big-header")
    }
}

const makeElementsGameFall = (e) => {
    const images = document.querySelectorAll(".students-works-container > img")
    images.forEach(img => {
        if (img.classList.contains("show")) {
            img.classList.toggle("show")
        } else {
            img.classList.add("show")
        }
    })
}


const moveBlender = (e) => {
    blenderYPosition = e.touches[0].clientY - blender.clientHeight / 2
    blenderXPosition = e.touches[0].clientX - blender.clientWidth / 2
    e.currentTarget.style.top = `${blenderYPosition}px`
    e.currentTarget.style.left = `${blenderXPosition}px`
    if (document.querySelector(".stuck")){
        let stuck = document.querySelector(".stuck")
        stuck.style.transition = "none"
        stuck.style.top = `${e.touches[0].clientY }px`
        stuck.style.left = `${e.touches[0].clientX - stuck.clientWidth / 2}px`  
    }
}


const checkIfElementIsInBlender = (xImg, yImg, yBlender, xbBlender) => {
    // console.log(xImg, yImg, xbBlender, yBlender)
    if (xImg >= xbBlender + 20) {
        show.classList.remove('show')
        show.classList.add('stuck')
        show.style.top = `${xImg}px`
        clearInterval(refreshIntervalId)
        console.log("CROSS", xImg, xbBlender )
    }
}


const checkPositionOfFallingImg = (e) => {
    refreshIntervalId = setInterval(() => {
        positionTopImg = parseInt(`${window.getComputedStyle(show).getPropertyValue('top')}`,
            10)
        checkIfElementIsInBlender(positionTopImg, positionLeftImg, blenderXPosition,
            blenderYPosition)
    }, 100)
}

allSidesBtn.forEach(btn => btn.addEventListener("click", e => e.currentTarget.classList.toggle("open-btn")))
document.addEventListener("scroll", reduceTitle)
playBtn.addEventListener('click', makeElementsGameFall)
blender.addEventListener("touchmove", moveBlender)
show.addEventListener('transitionrun', checkPositionOfFallingImg);
show.addEventListener('transitionend', (e) => {
    console.log("end")
    clearInterval(refreshIntervalId)
});



//not clean but working
const blender = document.querySelector('#blender')
const playBtn = document.querySelector(".btn")
const allSidesBtn = document.querySelectorAll(".side-btn")
let blenderYPosition = parseInt(`${blender.style.top}`, 10)
let blenderXPosition = parseInt(`${blender.style.left}`, 10)
// let imagesPositions =[]
const images = document.querySelectorAll(".students-works-container > img")
let counterTop = 0
let counterArrayTop = [-50, -30, 0, 80]
let counterLeft = 0
let counterArrayLeft = [20, -35, 80, 35]

const reduceTitle = (e) => {
    if (document.querySelector(".big-header")) {
        document.querySelector(".big-header").classList.remove("big-header")
    }
}

const fixFallingImg = (image, xImg, yImg) => {
    imagesPositions = []
    image.classList.remove('show')
    image.classList.add('stuck')
    image.style.transition = "none"
    image.style.top = `${xImg + 110}px`
    image.style.left = `${yImg}px`
    counterTop += 20
    counterArrayTop.push(counterTop)
    counterLeft += 20
    counterArrayLeft.push(counterLeft)
    // console.log(xImg, blenderXPosition, yImg, blenderYPosition)
    // imagesPositions.push(xImg - blenderXPosition)
    // imagesPositions.push(yImg - blenderYPosition) 
    // console.log(imagesPositions)
}

//MOOVE BLENDER AND IMAGES
const moveStuckImgs = (e) => {
    if (document.querySelector(".stuck")) {
        let allStuck = document.querySelectorAll(".stuck")
        allStuck.forEach(stuck => {
            let xImg = parseInt(`${stuck.style.top}`, 10)
            let yImg = parseInt(`${stuck.style.left}`, 10)
            xImg = e.touches[0].clientY - xImg
            yImg = e.touches[0].clientX - yImg

            stuck.style.top = `${e.touches[0].clientY - xImg}px`
            stuck.style.left = `${e.touches[0].clientX - yImg}px`
        })
    }
}

const moveBlender = (e) => {
    blenderYPosition = e.touches[0].clientY - blender.clientHeight / 2
    blenderXPosition = e.touches[0].clientX - blender.clientWidth / 2
    e.target.style.top = `${blenderYPosition}px`
    e.target.style.left = `${blenderXPosition}px`
    // moveStuckImgs(e)
    if (document.querySelector(".stuck")) {
        let allStuck = document.querySelectorAll(".stuck")
        allStuck.forEach((stuck, index) => {

            // let xImg = parseInt(`${stuck.style.top}`, 10)
            // let yImg = parseInt(`${stuck.style.left}`, 10)
            // let computxImg = imagesPositions[0]
            // let computyImg = imagesPositions[1]
            // console.log("info", imagesPositions[0], imagesPositions[1])

            // console.log(e.touches[0].clientY - stuck.clientHeight / 2, e.touches[0].clientX - stuck.clientWidth / 2)
            stuck.style.top = `${((e.touches[0].clientY - counterArrayTop[index]) + 100)}px`
            stuck.style.left = `${((e.touches[0].clientX - counterArrayLeft[index]) - stuck.clientWidth / 2)}px`

        })
    }
}


const checkIfElementIsInBlender = (e) => {
    let refreshIntervalId = 0

    refreshIntervalId = setInterval(() => {
        let positionTopImg = 0
        let positionLeftImg = 0
        positionTopImg = parseInt(`${window.getComputedStyle(e.target).getPropertyValue('top')}`, 10)
        positionLeftImg = e.target.x
        let compareValue = 0
        if (blenderXPosition <= 0) {
            compareValue = -blenderXPosition
        } else {
            compareValue = blenderXPosition
        }
        // && compareValue - positionLeftImg <= 10
        if (positionTopImg >= blenderYPosition + 100) {
            fixFallingImg(e.target, positionTopImg, positionLeftImg)
            clearInterval(refreshIntervalId)
        }
    }, 30)

    e.target.addEventListener('transitionend', e => {
        clearInterval(refreshIntervalId)
    });
}

const makeElementsGameFall = (e) => {
    images.forEach(img => {
        if (img.classList.contains("show")) {
            img.classList.toggle("show")
        } else {
            img.classList.add("show")
        }
        img.addEventListener('transitionrun', checkIfElementIsInBlender);
    })
}





allSidesBtn.forEach(btn => btn.addEventListener("click", e => e.currentTarget.classList.toggle("open-btn")))
document.addEventListener("scroll", reduceTitle)
playBtn.addEventListener('click', makeElementsGameFall)
blender.addEventListener("touchmove", moveBlender)
