const blender = document.querySelector('#blender')
const playBtn = document.querySelector(".btn")

let blenderYPosition = parseInt(`${blender.style.top}`, 10)
let blenderXPosition = parseInt(`${blender.style.left}`, 10)
const images = document.querySelectorAll(".students-works-container > img")
let counterArrayTop = [-50, -30, 0, 80]
let counterArrayLeft = [20, -35, 80, 35]
let counter = 0






const enableScrolling = () => {
    if (document.querySelector(".stop-scrolling")) {
        document.querySelector(".stop-scrolling").classList.remove("stop-scrolling")
    }
}



const fixFallingImg = (image, xImg, yImg) => {
    image.classList.remove('show')
    image.classList.add('stuck')
    image.style.transition = "none"
    image.style.top = `${xImg + 110}px`
    image.style.left = `${yImg}px`
}

//MOOVE BLENDER AND IMAGES
const moveStuckImgs = (e) => {
    if (document.querySelector(".stuck")) {
        let allStuck = document.querySelectorAll(".stuck")
        allStuck.forEach(stuck => {
            let xImg = parseInt(`${stuck.style.top}`, 10)
            let yImg = parseInt(`${stuck.style.left}`, 10)
            xImg = e.touches[0].clientY - xImg
            yImg = e.touches[0].clientX - yImg
            stuck.style.top = `${e.touches[0].clientY - xImg}px`
            stuck.style.left = `${e.touches[0].clientX - yImg}px`
        })
    }
}


const moveBlender = (e) => {
    blenderYPosition = e.touches[0].clientY - blender.clientHeight / 2
    blenderXPosition = e.touches[0].clientX - blender.clientWidth / 2
    e.target.style.top = `${blenderYPosition}px`
    e.target.style.left = `${blenderXPosition}px`
}

const moveAll = (e) => {
    if (document.querySelector(".stuck")) {
        window.requestAnimationFrame(() => {
            moveBlender(e)
            let allStuck = document.querySelectorAll(".stuck")
            allStuck.forEach((stuck, index) => {
                stuck.style.top = `${((e.touches[0].clientY - counterArrayTop[index]) + 100)}px`
                stuck.style.left = `${((e.touches[0].clientX - counterArrayLeft[index]) - stuck.clientWidth / 2)}px`

            })
            if (allStuck.length >= images.length) {
                enableScrolling()
            }
        })
    } else {
        window.requestAnimationFrame(() => {
            moveBlender(e)
        })
    }
}




const checkIfElementIsInBlender = (e, index) => {
    let refreshIntervalId = 0

    refreshIntervalId = setInterval(() => {
        let positionTopImg = 0
        let positionLeftImg = 0
        positionTopImg = parseInt(`${window.getComputedStyle(e.target).getPropertyValue('top')}`, 10)
        positionLeftImg = e.target.x
        let compareValue = 0
        if (blenderXPosition <= 0) {
            compareValue = -blenderXPosition
        } else {
            compareValue = blenderXPosition
        }
        // compareValue = compareValue - positionLeftImg
        // if (compareValue <= 0) {
        //     compareValue = -compareValue
        // } else {
        //     compareValue = compareValue
        // }
        // if (compareValue + positionLeftImg < 50 && compareValue - positionLeftImg > -50){
        //     compareValue = true
        // } else {
        //     compareValue = false
        // }
        // console.log(compareValue)
        if (positionTopImg >= blenderYPosition + 300) {
            fixFallingImg(e.target, positionTopImg, positionLeftImg)
            clearInterval(refreshIntervalId)
        }
    }, 10)

    e.target.addEventListener('transitionend', e => {
        clearInterval(refreshIntervalId)
        if(e.target.id = "img3"){
            enableScrolling()
        }

    });
}

const makeElementsGameFall = (e) => {
    images.forEach((img) => {
        if (img.classList.contains("show")) {
            img.classList.toggle("show")
        } else {
            img.classList.add("show")
        }
        img.addEventListener('transitionrun',  checkIfElementIsInBlender);
    })
}


playBtn.addEventListener('click', makeElementsGameFall)
blender.addEventListener("touchmove", moveAll)











