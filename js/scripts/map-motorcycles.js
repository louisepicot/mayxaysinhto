const bullets = document.querySelectorAll(".bullet")
const bulletsArr = Array.from(document.querySelectorAll(".bullet"))
const allSidesBtn = document.querySelectorAll(".side-btn")
const map = document.querySelector('.map-container')

const isInViewport = (elem) => {
    let bounding = elem.getBoundingClientRect();
    return (
        bounding.top <= 300 && bounding.bottom >= 400
    );
};


window.addEventListener("scroll", e => {
    window.requestAnimationFrame(() => {
        if (isInViewport(map)) {
            document.querySelectorAll(".left").forEach(bullet => {
                bullet.classList.remove("left")
            })
            document.querySelectorAll(".right").forEach(bullet => {
                bullet.classList.remove("right")
            })
        } else {
            bulletsArr.forEach(bullet => {
                bullet.classList.add("left")
            })
        }
    })

}, false)


allSidesBtn.forEach(btn => btn.addEventListener("click", e => e.currentTarget.classList.toggle("open-btn")))